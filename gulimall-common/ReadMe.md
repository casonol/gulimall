

 集成nacos配置中心  
 1）引入依赖
 ```$java
   <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
    </dependency>
 ```
 2) 创建一个 bootstrap.properties文件 
   并配置 
   ```properties
     spring.application.name=gulimall-coupon # 服务名
     spring.cloud.nacos.config.server-addr=192.168.10.120:8848 #nacos服务地址
   ```
 3）需要给配置中心默认添加一个叫数据集（Data Id）-》gulimall-coupon.properties
    默认规则是：应用名.properties
 4) 给 应用名.properties 添加任何配置
 5) 动态获取配置：
   @RefreshScope: 动态刷新配置 + @Value('{配置项的名}') ： 获取动态配置的值
   如果配置中心和当前应用的配置文件中都配置了相同的项，优先使用配置中心的值
 
2、细节 
   1）命名空间：配置隔离 默认是：public
      1、开发、测试、生产： 利用命名空间来做环境隔离
        注意：在 gulimall-coupon.properties 配置上，需要使用那个命名空间下的配置
        spring.cloud.nacos.config.namespace=56048873-f96f-4602-ac69-6c6f8f9117bf
      2、每一个微服务之间互相隔离，每个微服务都创建自己的命名空间，只加载自己命名空间下的所有配置
   2）配置集：所有配置的集合
   3）配置集id：类似文件名 
      Data ID： 类似文件名
   4）配置分组：
       默认所有的配置集都属于：DEFAULT_GROUP
  每个微服务创建自己的命名空间，使用配置分组区分环境，dev，test，prop
  
  同时加载多个配置集
  1）微服务任何配置信息，任何配置文件都可以放在配置中心
  2）只需要在 bootstrap.properties 说明加载配置中心的哪些配置文件即可
  3) 以前SpringBoot任何方法从配置文件中获取值，都能使用。比如：
    @value @ConfigureationProperties 等等。。。
    配置中心有的优先使用配置中心
    

整合 Mybatis-plus 
  1）导入依赖
  ```xml
      <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.2.0</version>
        </dependency>
  ``` 
  2) 配置 
    1、配置数据源
       1、导入数据库驱动
       2、在application.yml 配置数据源相关信息
    2、配置Mybatis-plus
       1、使用@mapperScan
       2、告诉Mybatis-plus，sql映射文件的位置
  3) Mybatis-plus 逻辑删除
    1、配置全局的逻辑删除规则（省略）
    2、配置逻辑删除的组件Bean（省略）
    3、在表逻辑删除的字段对应的实体 参数上加上注解：@TableLogic
    
  分布式 阿里云对象存储功能集成
1、引入依赖
 ```xml
        <dependency>
             <groupId>com.alibaba.cloud</groupId>
             <artifactId>spring-cloud-starter-alicloud-oss</artifactId>
         </dependency>
```
2、在yml文件中配置 
```properties
 spring
   cloud:
      alicloud:
        access-key: xxx  # 密钥 id
        secret-key: xxx # 密钥key
        oss:
          endpoint: xxx # endpoint
```
3、项目中注入Bean 进行相关操作
   import com.aliyun.oss.OSSClient;
    @Autowired
    OSSClient ossClient; 
 直接使用
 ```java
      @Test 
      public void testUpload2() throws Exception{
          // 填写本地文件的完整路径。如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
          InputStream inputStream = new FileInputStream("E:\\temp\\img\\b.jpg");
          // 填写Bucket名称和Object完整路径。Object完整路径中不能包含Bucket名称。
          ossClient.putObject("gulimall-cason", "b.jpg", inputStream);
  
          // 关闭OSSClient。
          ossClient.shutdown();
          System.err.println("上传完成....");
      }
```

后端校验使用 JSR303
 1、给Bean添加校验注解  javax.validation包中的注解 ，并定义自己的message提示
 2、在Controller对应的Bean上 添加 @Valid 注解
    效果： 校验错误会有默认的响应
 3、给校验的Bean后紧跟一个 BindingResult, 就可以获取到校验的结果
 4、JSR303 分组校验（多场景复杂校验）
   1）@NotBlank(message = "品牌名不能为空",groups = {AddGroup.class,UpdateGroup.class}) 
   校验注解标注什么情况需要进行校验
   2）在controller请求参数中标注 @Validated(AddGroup.class)
   3）默认没用指定分组校验的 @NotBlank 在@Valid 这种情况生效
       指定了分组校验的 @NotBlank 在 Validated下生效
 5、自定义校验
   1）编写一个自定义的校验注解
   2）编写一个自定义的校验器
   3）关联 自定义的校验器与 自定义的校验注解
       @Documented
       @Constraint(validatedBy = { ListValueConstraintValidator.class }) // 可以指定多个校验器，适配不同的校验类型
       @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
       @Retention(RUNTIME)
       public @interface ListValue {
   
统一异常处理  （@ControllerAdvice）
 错误码和错误信息定义类 
 1、错误码定义规则为5个数字
 2、前两位表示业务场景，最后三位表示错误码  例如：100001 。 10通用 001：系统未知异常
 3、维护错误码后需要维护错误描述。将他们定义为 枚举形式
 4、错误码列表 
   10 通用 
      001 参数格式校验 
   11-商品
   12-订单
   13-购物车
   14-物流
   
整合elasticSearch 
 1、导入依赖  (注意es版本与springBoot兼容)
  <!-- 导入 es的 rest-high-level-client 的api-->
          <dependency>
              <groupId>org.elasticsearch.client</groupId>
              <artifactId>elasticsearch-rest-high-level-client</artifactId>
              <version>7.4.2</version>
          </dependency> 
 2、编写配置类 初始化es实例 
     @Bean
        public void esRestClient(){
            RestHighLevelClient client = new RestHighLevelClient(
                    RestClient.builder(
                            new HttpHost("192.168.10.120",9200,"http")));
        }
 3、操作es 参照官方文档操作 https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-low-usage-requests.html#java-rest-low-usage-request-options