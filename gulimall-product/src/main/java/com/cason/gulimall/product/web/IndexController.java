package com.cason.gulimall.product.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    /**
     * 首页
     * @return
     */
    @GetMapping({"/","/index.html"})
    public String indexPage(){

        //TODO 查出一级分类


        return "index";
    }
}
