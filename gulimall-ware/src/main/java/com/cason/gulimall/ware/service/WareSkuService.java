package com.cason.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cason.common.to.SkuHasStockVo;
import com.cason.common.utils.PageUtils;
import com.cason.gulimall.ware.entity.WareSkuEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author casonol
 * @email casonol@163.com
 * @date 2021-03-12 13:38:25
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    List<SkuHasStockVo> getSkusHasStock(List<Long> skuIds);
}

