package com.cason.gulimall.ware.dao;

import com.cason.gulimall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品库存
 * 
 * @author casonol
 * @email casonol@163.com
 * @date 2021-03-12 13:38:25
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {

    void addStock(@Param("skuId") Long skuId,
                  @Param("wareId") Long wareId,
                  @Param("skuNum") Integer skuNum);

    Long getSkuStock(@Param("skuId")Long skuId);
}
