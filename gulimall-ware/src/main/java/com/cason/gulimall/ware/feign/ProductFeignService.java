package com.cason.gulimall.ware.feign;

import com.cason.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("gulimall-gateway")
public interface ProductFeignService {

    /**
     *
     *
     *     1)、让所有请求过网关
     *       1、@FeignClient("gulimall-gateway")； 给 gulimall-gateway 所在的机器发请求
     *       2、配置好请求路径  /api/product/spuinfo/info/{skuId}
     *     2)、直接给服务发请求处理
     *       1、@FeignClient("gulimall-product")； 给 gulimall-product 所在的机器发请求
     *       2、配置好请求路径  /product/spuinfo/info/{skuId}
     * @param skuId
     * @return
     */
    @RequestMapping("/api/product/spuinfo/info/{skuId}")
    R info(@PathVariable("skuId") Long skuId);
}
