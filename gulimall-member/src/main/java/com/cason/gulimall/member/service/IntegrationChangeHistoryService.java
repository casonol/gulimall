package com.cason.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cason.common.utils.PageUtils;
import com.cason.gulimall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author casonol
 * @email casonol@163.com
 * @date 2021-03-12 13:31:07
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

