package com.cason.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cason.common.utils.PageUtils;
import com.cason.gulimall.member.entity.MemberEntity;

import java.util.Map;

/**
 * 会员
 *
 * @author casonol
 * @email casonol@163.com
 * @date 2021-03-12 13:31:07
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

