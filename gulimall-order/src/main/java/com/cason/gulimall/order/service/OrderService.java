package com.cason.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cason.common.utils.PageUtils;
import com.cason.gulimall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author casonol
 * @email casonol@163.com
 * @date 2021-03-12 13:35:51
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

