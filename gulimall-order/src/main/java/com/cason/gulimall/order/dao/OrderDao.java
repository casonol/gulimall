package com.cason.gulimall.order.dao;

import com.cason.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author casonol
 * @email casonol@163.com
 * @date 2021-03-12 13:35:51
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
