package com.cason.gulimall.search.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MallElasticSearchConfig {

    public static final RequestOptions COMMON_OPTIONS;
    static {
        RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
//        builder.addHeader("Authorization", "Bearer " + TOKEN);
//        builder.setHttpAsyncResponseConsumerFactory(
//                new HttpAsyncResponseConsumerFactory
//                        .HeapBufferedResponseConsumerFactory(30 * 1024 * 1024 * 1024));
        COMMON_OPTIONS = builder.build();
    }

    /**
     * 初始化 es实例
     */
    @Bean
    public RestHighLevelClient esRestClient() {
        RestClientBuilder  builder = RestClient.builder(
                new HttpHost("192.168.10.120", 9200, "http"));
        RestHighLevelClient client = new RestHighLevelClient(builder);
    /*    return new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("192.168.10.120", 9200, "http")));*/

        return client;
    }
}
