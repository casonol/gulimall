package com.cason.gulimall.search.constant;

/**
 * @author: zhangshuaiyin
 * @date: 2021/3/11 15:22
 */
public class EsConstant {

    //在es中的索引
    public static final String PRODUCT_INDEX = "gulimall_product";

    public static final Integer PRODUCT_PAGE_SIZE = 16;
}
