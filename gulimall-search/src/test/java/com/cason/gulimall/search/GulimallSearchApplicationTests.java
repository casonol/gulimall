package com.cason.gulimall.search;

import com.alibaba.fastjson.JSON;
import com.cason.gulimall.search.config.MallElasticSearchConfig;
import lombok.Data;
import lombok.ToString;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallSearchApplicationTests {

    @Resource
    private RestHighLevelClient client;


    /**
     * 检索数据
     */
    @Test
    public void searchDate() throws IOException {
        // 1、创建检索请求
        SearchRequest searchRequest = new SearchRequest();
        // 指定索引
        searchRequest.indices("bank");

        // 指定DSL 检索条件
        // SearchSourceBuilder sourceBuilder // 封装条件
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 1.1 构建检索条件
        sourceBuilder.query(QueryBuilders.matchQuery("address","mill"));
        // 1.2) 按照年龄的值分布聚合
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
        sourceBuilder.aggregation(ageAgg);
        // 1.3) 计算平均薪资
        AvgAggregationBuilder balanceAvg = AggregationBuilders.avg("balanceAvg").field("balance");
        sourceBuilder.aggregation(balanceAvg);

        System.err.println("检索条件：" + sourceBuilder.toString());

        searchRequest.source(sourceBuilder);

        // 2、执行检索
        SearchResponse searchResponse =
                client.search(searchRequest, MallElasticSearchConfig.COMMON_OPTIONS);
        // 3、分析结果
        System.err.println("检索响应结果："+searchResponse.toString());

        // 3.1) 获取所有查到的数据
        SearchHits searchHits = searchResponse.getHits();
        SearchHit[] hits = searchHits.getHits();
        for (SearchHit hit : hits) {
            Account account = JSON.parseObject(hit.getSourceAsString(), Account.class);
            System.err.println(account.toString());
        }

        // 3.2) 获取检索的分析信息
        Aggregations aggregations = searchResponse.getAggregations();
        /*for (Aggregation aggregation : aggregations) {
            System.err.println("当前聚合名称："+aggregation.getName());
        }*/
        Terms ageAgg1 = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : ageAgg1.getBuckets()) {
            String keyAsString = bucket.getKeyAsString();
            System.err.println("年龄：" + keyAsString +"--->" + bucket.getDocCount());
        }
        Avg  balanceAvg1 = aggregations.get("balanceAvg");
        System.err.println("平均薪资："+ balanceAvg1.getValue());

    }

    /**
     * 给 es 存储数据
     */
    @Test
    public void indexDate() throws IOException {
        IndexRequest indexRequest = new IndexRequest("users");
        // 准备数据给es
        indexRequest.id("1"); // 数据id
        User user = new User();
        user.setUserName("zhangsan");
        user.setAge(18);
        user.setGender("男");
        String userJson = JSON.toJSONString(user);
        indexRequest.source(userJson, XContentType.JSON); // 要保存的数据

        // 执行操作
        IndexResponse index = client.index(indexRequest, MallElasticSearchConfig.COMMON_OPTIONS);
        // 提取有用的响应数据
        System.err.println(index);
    }

    @Data
    class User{
        private String userName;
        private String gender;
        private Integer age;
    }

    @Data
    @ToString
    static class Account {
        private int account_number;
        private int balance;
        private String firstname;
        private String lastname;
        private int age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;
    }

    /**
     * 测试连接es
     */
    @Test
    public void testEs() {
        System.err.println(client);
    }


    @Test
    public void contextLoads() {
    }

}
