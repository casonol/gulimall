package com.cason.gulimall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@Configuration
public class MallCrosConfiguration {

    /**
     * 配置跨域请求
     * @return
     */
    @Bean // 加入容器中
    public CorsWebFilter corsWebFilter(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfig = new CorsConfiguration();
        // 配置跨域
        corsConfig.addAllowedHeader("*"); // 请求头
        corsConfig.addAllowedMethod("*"); // 请求方法
        corsConfig.addAllowedOrigin("*"); // 请求来源
        corsConfig.setAllowCredentials(true); // 是否允许携带cookie


        source.registerCorsConfiguration("/**",corsConfig);

        return new CorsWebFilter(source);
    }
}
